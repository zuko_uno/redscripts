-- HOW TO USE

SELECT public.retargeting_deti_vk();
SELECT public.retargeting_deti_ok();
SELECT public.retargeting_deti_mm();



-- VKONTAKTE
CREATE OR REPLACE FUNCTION public.retargeting_deti_vk()
  RETURNS record
  LANGUAGE sql
AS $function$
  WITH u AS (
    SELECT DISTINCT uid
      FROM fpayers_vk_210916 -- Название сгенерированной таблицы с отвалившимися платящими в БД.
  ),
  pu AS (
    SELECT DISTINCT uid
      FROM visit
        WHERE ts >= '2016-09-21' AND ts <= '2016-10-02' AND uid IN (SELECT uid FROM u) AND network = 'vk'
  ),
  uzers AS (
    SELECT count(*) AS x
      FROM u
  ),
  reactivations AS (
    SELECT count(distinct uid) AS x
      FROM pu
  ),
  ad_payers AS (
    SELECT count(distinct uid) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'vk'
  ),
  ad_payments AS (
    SELECT count(*) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'vk'
  ),
  ad_paid AS (
    SELECT sum(after_cut) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'vk'  
  )
  SELECT uzers.x as targeted, reactivations.x as reactivated, ad_payers.x as payers, ad_payments.x as payments, ad_paid.x as paid, round(ad_paid.x / reactivations.x, 2) as epv
      FROM uzers, reactivations, ad_payers, ad_payments, ad_paid
$function$;



-- ODNOKLASSNIKI
CREATE OR REPLACE FUNCTION public.retargeting_deti_ok()
  RETURNS record
  LANGUAGE sql
AS $function$
  WITH u AS (
    SELECT DISTINCT uid
      FROM fpayers_ok_210916 -- Название сгенерированной таблицы с отвалившимися платящими в БД.
  ),
  pu AS (
    SELECT DISTINCT uid
      FROM visit
        WHERE ts >= '2016-09-21' AND ts <= '2016-10-02' AND uid IN (SELECT uid FROM u) AND network = 'ok'
  ),
  uzers AS (
    SELECT count(*) AS x
      FROM u
  ),
  reactivations AS (
    SELECT count(distinct uid) AS x
      FROM pu
  ),
  ad_payers AS (
    SELECT count(distinct uid) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'ok'
  ),
  ad_payments AS (
    SELECT count(*) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'ok'
  ),
  ad_paid AS (
    SELECT sum(after_cut) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'ok'  
  )
  SELECT uzers.x as targeted, reactivations.x as reactivated, ad_payers.x as payers, ad_payments.x as payments, ad_paid.x as paid, round(ad_paid.x / reactivations.x, 2) as epv
      FROM uzers, reactivations, ad_payers, ad_payments, ad_paid
$function$;



-- MOI MIR
CREATE OR REPLACE FUNCTION public.retargeting_deti_mm()
  RETURNS record
  LANGUAGE sql
AS $function$
  WITH u AS (
    SELECT DISTINCT uid
      FROM fpayers_mm_210916 -- Название сгенерированной таблицы с отвалившимися платящими в БД.
  ),
  pu AS (
    SELECT DISTINCT uid
      FROM visit
        WHERE ts >= '2016-09-21' AND ts <= '2016-10-02' AND uid IN (SELECT uid FROM u) AND network = 'mm'
  ),
  uzers AS (
    SELECT count(*) AS x
      FROM u
  ),
  reactivations AS (
    SELECT count(distinct uid) AS x
      FROM pu
  ),
  ad_payers AS (
    SELECT count(distinct uid) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'mm'
  ),
  ad_payments AS (
    SELECT count(*) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'mm'
  ),
  ad_paid AS (
    SELECT sum(after_cut) AS x
      FROM payment
        WHERE ts >= '2016-09-21' AND uid IN (SELECT uid FROM pu) AND network = 'mm'  
  )
  SELECT uzers.x as targeted, reactivations.x as reactivated, ad_payers.x as payers, ad_payments.x as payments, ad_paid.x as paid, round(ad_paid.x / reactivations.x, 2) as epv
      FROM uzers, reactivations, ad_payers, ad_payments, ad_paid
$function$;
