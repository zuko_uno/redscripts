#/bin/bash

#ВК: ./2_export_lists.sh fpayers_vk_210916 1 999999
#OK: ./2_export_lists.sh fpayers_ok_210916 1 999999
#MM: ./2_export_lists.sh fpayers_mm_210916 1 999999

#Дописать в начало ОК файла – http://ok.ru/game/nanofarm

DPATH="../data/."
TNAME=$1

# Ферма
DFILE="/tmp/farm_${TNAME}.csv"
ssh rstat2 "psql farm farm -c \"\copy (select uid from $TNAME) to STDOUT (delimiter '|', format 'csv')\" > $DFILE"
scp rstat2:"$DFILE" $DPATH
ssh rstat2 "unlink $DFILE;"