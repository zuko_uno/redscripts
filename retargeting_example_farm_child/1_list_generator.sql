-- С помощью этих запросов создается таблица с отвалившимися платящими.

-- Ферма: ssh rstat2; psql farm farm


CREATE TABLE lvl50_vk AS (
  SELECT distinct uid
  FROM users
  WHERE level > 50 AND network = 'vk'
);

CREATE TABLE lvl50_ok AS (
  SELECT distinct uid
  FROM users
  WHERE level > 50 AND network = 'ok'
);

CREATE TABLE lvl50_mm AS (
  SELECT distinct uid
  FROM users
  WHERE level > 50 AND network = 'mm'
);



-- VKONTAKTE
-- VKONTAKTE
-- VKONTAKTE
-- VKONTAKTE

-- Шаг 1. Создаем таблицу со всеми платящими игры, вида: "uid,заплатил_всего,количество_платежей"
DROP TABLE allpayers_vk;
CREATE TABLE allpayers_vk AS (
WITH u AS (
  SELECT uid, sum(after_cut) AS summa, count(*) AS cnt
    FROM payment
      WHERE network = 'vk' and ts <= (now() - interval '760 day')
        GROUP BY uid
)
SELECT uid, summa, cnt
  FROM u
    ORDER BY summa DESC
);

-- Шаг 2. Создаем таблицу с платящими, у которых есть визит за последние 14 дней.
DROP TABLE activepayers_vk;
WITH p AS (
SELECT uid
  FROM allpayers_vk
)
SELECT distinct(uid)
  INTO activepayers_vk
    FROM visit
      WHERE ts >= (now() - interval '30 day') AND uid IN (SELECT uid FROM p) AND network = 'vk';

-- Шаг 3. Вычитаем Шаг 2 из Шага 1. Получаем платящих, у которых нет визита, за последние 30 дней.
-- Название таблицы очень важно и будет использоваться везде в дальнейшем.
-- Название формируется как fpayers_{сеть}_{дата генерации таблицы в формате ddmmyy}.
CREATE TABLE xrtg_all AS (
  SELECT *
    FROM allpayers_vk
      WHERE uid not IN (SELECT uid FROM activepayers_vk WHERE uid IS NOT NULL) AND uid IN (SELECT uid FROM lvl50_vk)
        ORDER BY summa DESC
);


CREATE TABLE xrtg_sms AS (
  SELECT * 
    FROM xrtg_all 
    ORDER BY random() 
    LIMIT 100
);







-- ODNOKLASSNIKI
-- ODNOKLASSNIKI
-- ODNOKLASSNIKI
-- ODNOKLASSNIKI

-- Шаг 1. Создаем таблицу со всеми платящими игры, вида: "uid,заплатил_всего,количество_платежей"
DROP TABLE allpayers_ok;
CREATE TABLE allpayers_ok AS (
WITH u AS (
  SELECT uid, sum(after_cut) AS summa, count(*) AS cnt
    FROM payment
      WHERE network = 'ok'
        GROUP BY uid
)
SELECT uid, summa, cnt
  FROM u
    ORDER BY summa DESC
);


-- Шаг 2. Создаем таблицу с платящими, у которых есть визит за последние 14 дней.
DROP TABLE activepayers_ok;
WITH p AS (
SELECT uid
  FROM allpayers_ok
)
SELECT distinct(uid)
  INTO activepayers_ok
    FROM visit
      WHERE ts >= (now() - interval '14 day') AND uid IN (SELECT uid FROM p) AND network = 'ok';


-- Шаг 3. Вычитаем Шаг 2 из Шага 1. Получаем платящих, у которых нет визита, за последние 30 дней.
-- Название таблицы очень важно и будет использоваться везде в дальнейшем.
-- Название формируется как fpayers_{сеть}_{дата генерации таблицы в формате ddmmyy}.
CREATE TABLE fpayers_ok_210916 AS (
  SELECT *
    FROM allpayers_ok
      WHERE uid not IN (SELECT uid FROM activepayers_ok WHERE uid IS NOT NULL) AND uid IN (SELECT uid FROM lvl50_ok)
        ORDER BY summa DESC
);









-- MOI MIR
-- MOI MIR
-- MOI MIR
-- MOI MIR


-- Шаг 1. Создаем таблицу со всеми платящими игры, вида: "uid,заплатил_всего,количество_платежей"
DROP TABLE allpayers_mm;
CREATE TABLE allpayers_mm AS (
WITH u AS (
  SELECT uid, sum(after_cut) AS summa, count(*) AS cnt
    FROM payment
      WHERE network = 'mm'
        GROUP BY uid
)
SELECT uid, summa, cnt
  FROM u
    ORDER BY summa DESC
);


-- Шаг 2. Создаем таблицу с платящими, у которых есть визит за последние 14 дней.
DROP TABLE activepayers_mm;
WITH p AS (
SELECT uid
  FROM allpayers_mm
)
SELECT distinct(uid)
  INTO activepayers_mm
    FROM visit
      WHERE ts >= (now() - interval '14 day') AND uid IN (SELECT uid FROM p) AND network = 'mm';


-- Шаг 3. Вычитаем Шаг 2 из Шага 1. Получаем платящих, у которых нет визита, за последние 30 дней.
-- Название таблицы очень важно и будет использоваться везде в дальнейшем.
-- Название формируется как fpayers_{сеть}_{дата генерации таблицы в формате ddmmyy}.
CREATE TABLE fpayers_mm_210916 AS (
  SELECT *
    FROM allpayers_mm
      WHERE uid not IN (SELECT uid FROM activepayers_mm WHERE uid IS NOT NULL) AND uid IN (SELECT uid FROM lvl50_mm)
        ORDER BY summa DESC
);




