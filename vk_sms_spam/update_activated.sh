#/bin/bash

# Скачивает с сервера список uid-ов юзеров, которые активировались после рассылки

DPATH="./activated.list"

DFILE="/tmp/xrtg_activated.list"

ssh rstat2 "psql farm farm -c \"\copy (

WITH reactivated AS (
    SELECT DISTINCT uid
      FROM visit
        WHERE ts >= '2017-09-01' AND ts <= '2017-10-30' AND uid IN (SELECT DISTINCT uid FROM xrtg_all) AND network = 'vk'
)
SELECT uid from reactivated

) to STDOUT (delimiter ',', format 'csv')\" > $DFILE"
scp rstat2:"$DFILE" $DPATH
ssh rstat2 "unlink $DFILE;"
