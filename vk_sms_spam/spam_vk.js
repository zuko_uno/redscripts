var request = require('request');
var async = require('async');
var fs = require('fs');
var JFile = require('jfile');

var threads = 20;
var done = 0;

var actv_l = new JFile("./activated.list").lines;
var sent_l = new JFile("./sent.list").lines;
var errs_l = new JFile("./errors.list").lines;
var targ_l = new JFile("./targets.list").lines;


targ_l = targ_l.filter(function(x) {
	return ((sent_l.indexOf(x) < 0) || (actv_l.indexOf(x) < 0) || (errs_l.indexOf(x) < 0));
});

function finish(uid, kind, msg, next) {
	done += 1;
	if (kind == 'error') {
		console.log(msg);
		fs.appendFile('errors.log', "UID: " + uid + " " + msg + '\n\n\n\n', function(err) {});
		fs.appendFile('errors.list', uid + "\n", function(err) {});
	}

	if (kind == 'success') {
		console.log("SENT: ", uid);
		fs.appendFile('sent.list', msg + "\n", function(err) {});
	}

	if (done % 100 == 0) {
		console.log("Done: ", done);
	}

	next();
}

function send_sms(task, next) {
	api_url = "https://api.vk.com/method/secure.sendSMSNotification?access_token=dc180ab8dc180ab8dcd9e28abddc3c784addc18dc180ab88560853f10f8c8ad4606ecc2&v=5.67&client_secret=QbW7f465zIEnnrElh9dw&message=HAHO-HOBOCTb%3A+https%3A%2F%2Fvk.cc%2F76vKU8&user_id=" + task.uid;

	request({
		url: api_url
	}, function(error, response, body) {
		if (error) {
			finish(task.uid, 'error', 'HTTP REQUEST ERROR:\n' + error + '\n' + response + '\n' + body + '\n\n\n', next);
			next();
		}
		if (!error && response.statusCode == 200) {
			try {
				r = JSON.parse(body);
			} catch (e) {
				finish(task.uid, 'error', "MALFORMED JSON:\n" + body + '\n\n\n', next);
				return;
			}

			if (r.response === 1) {
				finish(task.uid, 'success', task.uid, next);
			} else {
				finish(task.uid, 'error', "ERROR SENT:\n" + task.uid + "\n" + JSON.stringify(r, null, 4), next);
			}
		}
	});

};

queue = async.queue(send_sms, threads);

for (var j = 0; j < targ_l.length; j++) {
	queue.push({
		uid: targ_l[j]
	})
}

queue.drain = function() {
	console.log("ITS DONE.");
}