#/bin/bash

# Скачивает с сервера статистики список игроков, который можно использовать в ретаргетинге ВКонтакте.
# Использование: ./2_export_retargeting_list.bash {название таблицы} {заплатил минимум} {заплатил максимум} 
# Обычно генерим 3 списка - 0-30, 30-300, 300-999999. Файлы появятся в папке data в redhacks.
# Пример: ./2_export_retargeting_list.bash fpayers_ok_080416 100 999999 - в списке будут uid-ы игроков, которые заплатили меньше 30р

DPATH="../data/."
TNAME=$1
FROM=$2
UPTO=$3

# Ферма
DFILE="/tmp/farm_${TNAME}_${FROM}-${UPTO}.csv"
ssh rstat2 "psql farm farm -c \"\copy (select uid from $TNAME where summa >= $FROM and summa < $UPTO) to STDOUT (delimiter '|', format 'csv')\" > $DFILE"
scp rstat2:"$DFILE" $DPATH
ssh rstat2 "unlink $DFILE;"

# Эвил
# DFILE="/tmp/evil_${TNAME}_${FROM}-${UPTO}.csv"
# ssh rstat3 "psql -p 5435 evil evil -c \"\copy (select uid from $TNAME where summa >= $FROM and summa < $UPTO) to STDOUT (delimiter '|', format 'csv')\" > $DFILE"
# scp rstat3:"$DFILE" $DPATH
# ssh rstat3 "unlink $DFILE;"
