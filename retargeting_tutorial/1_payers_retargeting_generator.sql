-- С помощью этих запросов создается таблица с отвалившимися платящими.

-- Эвил: ssh rstat3; psql -p 5435 evil evil
-- Ферма: ssh rstat2; psql farm farm

-- Шаг 1. Создаем таблицу со всеми платящими игры, вида: "uid,заплатил_всего,количество_платежей"
DROP TABLE allpayers_ok;
CREATE TABLE allpayers_ok AS (
WITH u AS (
  SELECT uid, sum(after_cut) AS summa, count(*) AS cnt
    FROM payment
      WHERE network = 'ok' -- Сеть ('vk' / 'vk')
        GROUP BY uid
)
SELECT uid, summa, cnt
  FROM u
    ORDER BY summa DESC
);


-- Шаг 2. Создаем таблицу с платящими, у которых есть визит за последние 14 дней.
DROP TABLE activepayers_ok;
WITH p AS (
SELECT uid
  FROM allpayers_ok
)
SELECT distinct(uid)
  INTO activepayers_ok
    FROM visit
      WHERE ts >= (now() - interval '14 day') AND uid IN (SELECT uid FROM p) AND network = 'ok';


-- Шаг 3. Вычитаем Шаг 2 из Шага 1. Получаем платящих, у которых нет визита, за последние 30 дней.
-- Название таблицы очень важно и будет использоваться везде в дальнейшем.
-- Название формируется как fpayers_{сеть}_{дата генерации таблицы в формате ddmmyy}.
CREATE TABLE fpayers_ok_220416 AS (
  SELECT *
    FROM allpayers_ok
      WHERE uid not IN (SELECT uid FROM activepayers_ok WHERE uid IS NOT NULL)
        ORDER BY summa DESC
);