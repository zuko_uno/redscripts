-- Делаем функции, для рассчета ROI ремаркетинг кампании. 
-- Функции сохранятся в БД и до окончания кампании их можно будет просто вызывать.

-- Шпаргалка:
-- 1. Используется незанятое имя функции (занятые можно посмотреть в глобальной таблице кампаний, в гуглдокс)?
-- 2. Используется правильное название таблицы с отвалившимися платящими? (мы генерировали ее на предыдущих этапах)
-- 3. Используем дату, в которую мы начали крутить рекламу по этому списку? (обычно это тот же день, в который генерировали список)
-- 4. Используем дату, в которую кампания будет завершена?
-- 5. В запросах используется правильная сеть ? (ok / vk)



-- Шаг 1. Создаем основную функцию, которая считает ROI-статистику для всей кампании.
CREATE OR REPLACE FUNCTION public.retargeting_roi_25()
  RETURNS record
  LANGUAGE sql
AS $function$
  WITH u AS (
    SELECT DISTINCT uid
      FROM fpayers_ok_300915 -- Название сгенерированной таблицы с отвалившимися платящими в БД.
  ),
  pu AS (
    SELECT DISTINCT uid
      FROM visit
        WHERE ts >= '2016-04-08' AND ts <= '2016-05-08' AND uid IN (SELECT uid FROM u) AND network = 'ok' AND refer like 'ad_%'
  ),
  uzers AS (
    SELECT count(*) AS x
      FROM u
  ),
  ad_visits AS (
    SELECT count(distinct uid) AS x
      FROM pu
  ),
  ad_payers AS (
    SELECT count(distinct uid) AS x
      FROM payment
        WHERE ts >= '2016-04-08' AND uid IN (SELECT uid FROM pu) AND network = 'ok'
  ),
  ad_payments AS (
    SELECT count(*) AS x
      FROM payment
        WHERE ts >= '2016-04-08' AND uid IN (SELECT uid FROM pu) AND network = 'ok'
  ),
  ad_paid AS (
    SELECT sum(after_cut) AS x
      FROM payment
        WHERE ts >= '2016-04-08' AND uid IN (SELECT uid FROM pu) AND network = 'ok'  
  )
  SELECT uzers.x as installs, ad_visits.x as adv, ad_payers.x as payers, ad_payments.x as payments, ad_paid.x as paid, round(ad_paid.x / ad_visits.x, 2) as epv
      FROM uzers, ad_visits, ad_payers, ad_payments, ad_paid
$function$;
