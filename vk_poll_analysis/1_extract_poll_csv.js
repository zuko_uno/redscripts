/*

Выкачивает данные запроса и генерирует csv для последующего импорта в rstat psql.

*/

/*
Авторизация ВКонтакте

Используем приложение orel450 - https://vk.com/editapp?id=2012119&section=options (можно и любое другое)

https://oauth.vk.com/authorize? 
 client_id=2012119&
 redirect_uri=http://orel450.ru/auz& 
 response_type=code& 
 v=5.27

https://oauth.vk.com/authorize?client_id=2012119&redirect_uri=http://orel450.ru/auz&response_type=code&v=5.27

получили код - 2c0e99957727eaf60e

https://oauth.vk.com/access_token? 
 client_id=2012119& 
 client_secret=LOLvqB5hThd0grSVDan3& 
 code=2c0e99957727eaf60e& 
 redirect_uri=http://orel450.ru/auz

https://oauth.vk.com/access_token?client_id=2012119&client_secret=LOLvqB5hThd0grSVDan3&redirect_uri=http://orel450.ru/auz&code=2c0e99957727eaf60e

получили access token - 9a97cd808c5eb384aedc1169f423901d6b54d91e2ff714f0e64ef4a6e7f8a5d4138792e3d0169a5a1c2a5
*/


//POLL
poll = {
	owner_id: '-28308494', //id группы
	id: '167727716', //число из кода виджета
	question: '?',
	answers: {
		'548691963': [1, ''],
		'548691964': [2, ''],
		'548691965': [3, ''],
		'548691966': [4, ''],
		'548691967': [5, ''],
		'548691968': [6, ''],
		'548691969': [7, ''],
		'548691970': [8, ''],
		'548691971': [9, ''],
		'548691972': [10, '']
	},
	queued: []
}

//REQUIRE
var request = require('request');
var async = require('async');
var sleep = require('sleep');

console.log('uid,aid,option');



function download_to_csv(task, next) {
	request({
		url: task.url
	}, function(error, response, body) {
		if (error) {
			console.log('POLL. CONNECTION ERROR.\nBody: ', body);
			next();
			return;
		}
		if (!error && response.statusCode == 200) {
			try {
				answer = JSON.parse(body).response[0];
			} catch (e) {
				console.log("MALFORMED JSON. Body: ", body, '\n');
				return;
			}

			// console.log(answer)

			if ((answer.users.count > 1000) && !(poll.queued.indexOf(task.answer_id) > -1)) {
				count_norm = Math.floor(answer.users.count / 1000) + 1;
				for (var j = 1; j < count_norm; j++) {
					offset = '&offset=' + (j * 1000);
					new_task = {
						url: task.url + offset,
						answer_id: task.answer_id,
						option: task.option,
						answer: task.answer
					}
					poll.queued.push(task.answer_id)
					queue.push(new_task)
				}
			}

			for (i = 0; i < answer.users.items.length; i++) {
				uid = answer.users.items[i]
				line = uid + ',' + task.answer_id + ',' + task.option
				console.log(line)
			}

			sleep.sleep(1);
			next();
		}
	})
}

queue = async.queue(download_to_csv, 1);
queue.drain = function() {
	process.exit(1)
}

Object.keys(poll.answers).forEach(function(key) {
	var a = poll.answers[key];

	answer_url = 'https://api.vk.com/method/polls.getVoters?' +
		'owner_id=' + poll.owner_id + '&poll_id=' + poll.id + '&answer_ids=' + key + '&count=1000&v=5.27' + '&access_token=9a97cd808c5eb384aedc1169f423901d6b54d91e2ff714f0e64ef4a6e7f8a5d4138792e3d0169a5a1c2a5'

	task = {
		url: answer_url,
		answer_id: key,
		option: a[0],
		answer: a[2]
	}

	queue.push(task)
});