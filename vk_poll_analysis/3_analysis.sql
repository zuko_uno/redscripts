-- Всего голосов
SELECT count(distinct(uid)), option
  FROM poll_farm_3
    GROUP BY option
      ORDER BY option ASC;


-- Из них платящих
SELECT count(distinct(uid)), option
  FROM poll_farm_3
    WHERE uid IN (SELECT uid FROM allpayers_vk WHERE uid IS NOT NULL)
      GROUP BY option
        ORDER BY option ASC;


-- Которые в сумме заплатили
SELECT sum(summa), option
  FROM poll_farm_3 INNER JOIN allpayers_vk ON (poll_farm_3.uid = allpayers_vk.uid)
    GROUP BY option
      ORDER BY option ASC;


-- Tiers
SELECT count(distinct(poll_farm_3.uid)), option
  FROM poll_farm_3 INNER JOIN allpayers_vk ON (poll_farm_3.uid = allpayers_vk.uid)
    WHERE summa >= 300
      GROUP BY option
        ORDER BY option ASC;


-- Tier 2 (30р - 300р)
-- Tier 3 (300р+)
-- % от всех голосовавших платящих













SELECT count(distinct(uid)), option
	FROM poll_farm_1
		GROUP BY option;

-- CREATE TEMPORARY TABLE WITH ALL PAYERS
CREATE TABLE allpayers_ok AS (
WITH u AS (
  SELECT uid, sum(after_cut) AS summa, count(*) AS cnt
    FROM payment
      WHERE network = 'ok'
        GROUP BY uid
)
SELECT uid, summa, cnt
  FROM u
    ORDER BY summa DESC
);

-- CREATE TEMPORARY TABLE WITH ALL PAYERS ACTIVE LAST MONTH
WITH p AS (
SELECT uid
  FROM allpayers_ok
)
SELECT distinct(uid)
  INTO activepayers_ok
    FROM visit
      WHERE ts >= (now() - interval '31 day') AND uid IN (SELECT uid FROM p) AND network = 'ok';

-- SUBTRACT TEMPORARY TABLES, TO GET ONLY INACTIVE PAYERS
CREATE TABLE fpayers_ok_051214 AS (
  SELECT *
    FROM allpayers_ok
      WHERE uid not IN (SELECT uid FROM activepayers_ok WHERE uid IS NOT NULL)
        ORDER BY summa DESC
);

-- TOP1000 INACTIVE PAYERS
CREATE TABLE fpayers_top1000_ok_051214 AS (
  SELECT *
    FROM fpayers_vk_051214
        ORDER BY summa DESC
        	LIMIT 1000
);