#/bin/bash

# Загружает в rstat результаты опроса
# example: ./2_upload_poll.bash poll_farm_3

POLLPATH="./poll.csv"
TABLENAME=$1


scp ./poll.csv rstat2:/tmp/poll.csv
ssh rstat2 "psql farm farm -c \"CREATE TABLE $TABLENAME (uid character varying(100), aid character varying(100), option smallint)\""
ssh rstat2 "psql farm farm -c \"COPY $TABLENAME FROM '/tmp/poll.csv' DELIMITER ',' CSV HEADER\""