WITH u AS (
  SELECT uid, sex, age
    FROM users
      WHERE network = 'mm'
),
p AS (
  SELECT uid, sum(after_cut) AS paid
    FROM payment
      WHERE uid IN (SELECT uid FROM u)
        GROUP BY uid
)
SELECT u.uid, sex, age, paid
  FROM u,p
    WHERE u.uid = p.uid;




-- КОЛИЧЕСТВО УСТАНОВОК РАЗНЫХ ПОЛОВ
WITH u AS (
  SELECT uid, sex, age
    FROM users
      WHERE network = 'mm'
)
SELECT sex, count(*)
  FROM u
    GROUP BY sex;





-- СРЕДНИЙ ЧЕК ДЛЯ ПОЛОВ
WITH r AS (
  SELECT distinct(uid)
    FROM visit
      WHERE network='ok' and is_new and ts >= '2015-03-01' and refer in ('ad_14799404','ad_14799403','ad_14799402','ad_14799401','ad_14799400','ad_14799399','ad_14799398','ad_14799397','ad_14799396','ad_14799395')
),
u AS (
  SELECT uid, sex, age
    FROM users
      WHERE network = 'ok' and uid in (select uid from r)
),
p AS (
  SELECT uid, sum(after_cut) AS paid
    FROM payment
      WHERE uid IN (SELECT uid FROM u)
        GROUP BY uid
),
packed AS (
  SELECT u.uid, sex, age, paid
    FROM u,p
      WHERE u.uid = p.uid
),
packed2 AS (
  SELECT sex, count(distinct uid) as players, sum(paid) as paid
    FROM packed
      GROUP BY sex
)
SELECT sex, players, paid, round(paid / players, 2) as avg_payment
  FROM packed2;

