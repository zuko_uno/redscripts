Всего игроков: 11971321
select count(*) from users where reg_ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm';

Всего платящих: 927522 (7.7%)
select count(distinct uid) from payment where ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm';

Сколько всего заплатили: 286735703.90 рублей
select sum(after_cut) from payment where ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm';

Сколько заплатил топ 10% платящих: 230608692.70 (80%)
with top10 as (
	select uid, sum(after_cut) as paid
	from payment 
	where ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm'
	group by uid
	order by paid desc
	limit 92752
)
select sum(paid) from top10;

Сколько заплатил топ 5% платящих: 192981333.70 (67%)
with top10 as (
	select uid, sum(after_cut) as paid
	from payment 
	where ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm'
	group by uid
	order by paid desc
	limit 46286
)
select sum(paid) from top10;

Сколько заплатил топ 1% платящих: 103920478.80 (36%)
with top10 as (
	select uid, sum(after_cut) as paid
	from payment 
	where ts > '2012-01-01' and network = 'vk' or network = 'ok' or network ='mm'
	group by uid
	order by paid desc
	limit 9275
)
select sum(paid) from top10;