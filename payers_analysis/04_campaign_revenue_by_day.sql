WITH u AS (
	select distinct uid
	  from users where adv_id in ('ad_15315647','ad_15315646','ad_15262134','ad_15262133','ad_15028108','ad_15028076','ad_14799694','ad_14759268','ad_14524274','ad_14344430','ad_13909252','ad_13909251','ad_13909249','ad_13909248','ad_15970627','ad_15970631','ad_15160889','ad_15160888','ad_15160887','ad_15160885','ad_15160884','ad_15160883','ad_15160882','ad_15970618','ad_15970619')
)
SELECT date_trunc('day', ts), sum(after_cut)
  FROM payment
	WHERE ts > '2015-06-01' AND network = 'ok' AND uid IN (SELECT uid FROM u)
	  GROUP BY date_trunc('day', ts) ORDER BY date_trunc('day', ts) ASC







-- EVIL ALL CAMPAIGNS
with r as (
  select distinct uid
    from visit
      where refer in ('ad_15315647','ad_15315646','ad_15262134','ad_15262133','ad_15028108','ad_15028076','ad_14799694','ad_14759268','ad_14524274','ad_14344430','ad_13909252','ad_13909251','ad_13909249','ad_13909248','ad_15970627','ad_15970631','ad_15160889','ad_15160888','ad_15160887','ad_15160885','ad_15160884','ad_15160883','ad_15160882','ad_15970618','ad_15970619') and is_new and network = 'ok' and ts >= '2015-02-12'
),
d as (
  select cast(ts as date) as day, count(distinct uid) as dau
    from visit
      where uid in (select uid from r) and network = 'ok' and ts >= '2015-02-12'
        group by day
),
p as (
  select cast(ts as date) as day, sum(after_cut) as summa
    from r
    inner join (
      select ts, uid, after_cut
        from payment
          where network = 'ok' and ts >= '2015-02-12'
    ) as p
    using (uid)
      group by day
)
select day, dau, summa, case when dau = 0 then 0 else summa / dau end as arppu
  from d
    inner join p
      using (day);