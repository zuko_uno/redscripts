WITH p AS (
	SELECT uid, sum(after_cut) as paid
	FROM payment
	WHERE network = 'ok'
	GROUP BY uid
)
SELECT p.uid, adv_id, paid, reg_ts
FROM users, p
WHERE users.uid = p.uid AND adv_id in ('ad_15446305','ad_15446304','ad_15446303','ad_15446302','ad_15446301','ad_15446300','ad_15446296','ad_15446298','ad_15446299','ad_15446292','ad_15446288','ad_15446294','ad_15446289','ad_15446291','ad_15446282','ad_15446286','ad_15446283','ad_15717935','ad_15717934','ad_15717931','ad_15717920','ad_15717909','ad_15717901','ad_15717900','ad_15717899','ad_15717898','ad_15717897','ad_15717888','ad_15717882','ad_15717864','ad_15717862','ad_15717861','ad_15717860','ad_15717859','ad_15717858','ad_15717857','ad_15717856','ad_15717852','ad_15717851','ad_15717845','ad_15717844','ad_15717841')
ORDER BY paid desc
LIMIT 100;

