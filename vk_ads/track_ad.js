// Get User Code:
// https://oauth.vk.com/authorize?client_id=2012119&display=page&redirect_uri=http://orel450.ru/callback&scope=ads,offline&response_type=code&v=5.74

// Get User Token:
// https://oauth.vk.com/access_token?client_id=2012119&client_secret=LOLvqB5hThd0grSVDan3&redirect_uri=http://orel450.ru/callback&code=be47e7ea5443f7c059

const fetch = require("node-fetch");
const async = require("async");
const fs = require("fs");
const util = require("util");

const token =
  "4e8dcd36eac462fe7de1e76eb755b4a77e15d6eb0556d1404fbe9dfdbe6a8b8ed93fce7ad9f2e0bddeefa";
const cab_id = 1600046766;
const ad_id = 41939921;
const today = new Date().toISOString().slice(0, 10);
const stat_url = `https://api.vk.com/method/ads.getStatistics?account_id=${cab_id}&ids_type=ad&ids=${ad_id}&period=day&date_from=${today}&date_to=0&access_token=${token}&v=5.74`;

const logFile = fs.createWriteStream("ad_41939921.log", {
  flags: "a"
});
const logStdout = process.stdout;

console.log = function() {
  logFile.write(util.format.apply(null, arguments) + "\n");
  logStdout.write(util.format.apply(null, arguments) + "\n");
};

function fetch_stats() {
  fetch(stat_url)
    .then(response => {
      response
        .json()
        .then(json => {
          let ts = new Date().toLocaleTimeString();
          s = json.response[0].stats[0];
          console.log(
            `${ts} ${s["reach"]} ${s["impressions"]} ${s["clicks"]} ${
              s["join_rate"]
            }`
          );
        })
        .catch(error => {
          console.log(error);
        });
    })
    .catch(error => {
      console.log(error);
    });
}

fetch_stats();
setInterval(fetch_stats, 120000);

function update_ad(done) {
  let changes = [
    {
      ad_id: ad_id,
      cpm: 5.7
    }
  ];
  let update_url = `https://api.vk.com/method/ads.updateAds?account_id=${cab_id}&data=${JSON.stringify(
    changes
  )}&access_token=${token}&v=5.74`;

  fetch(update_url)
    .then(response => {
      response.json().then(json => {
        console.log(JSON.stringify(json, null, 4));
      });
    })
    .catch(error => {
      console.log(error);
    });
}

update_ad();
