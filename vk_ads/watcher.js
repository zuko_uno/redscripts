const fetch = require("node-fetch");
const async = require("async");
const fs = require("fs");
const util = require("util");
const ads = require("./ads_db");

// Чтобы получить токен (по нему апи определяет, имеешь ли ты право совершать операции):
// Get User Code:
// https://oauth.vk.com/authorize?client_id=2012119&display=page&redirect_uri=http://orel450.ru/callback&scope=ads,offline&response_type=code&v=5.74
// Get User Token (подставить сюда User Code из предыдущей ссылки:
// https://oauth.vk.com/access_token?client_id=2012119&client_secret=LOLvqB5hThd0grSVDan3&redirect_uri=http://orel450.ru/callback&code=be47e7ea5443f7c059
// Я просто вставлял ссылку в строку браузера и из нее же забирал токен после редиректа.

const token =
  "4e8dcd36eac462fe7de1e76eb755b4a77e15d6eb0556d1404fbe9dfdbe6a8b8ed93fce7ad9f2e0bddeefa";

const state = {};

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

getNextBid = ad => {
  if (ad.currentBid == 0) {
    return ad.bids[0];
  } else {
    let pos = ad.bids.indexOf(ad.currentBid);
    let nextPos = pos == ad.bids.length - 1 ? 0 : pos + 1;
    return ad.bids[nextPos];
  }
};

function replacer(key, value) {
  if (key == "file") return undefined;
  else return value;
}

async function updateAdBid(ad) {
  let changes;
  if (ad.cpc) {
    changes = [
      {
        ad_id: ad.id,
        cpc: ad.currentBid
      }
    ];
  } else {
    changes = [
      {
        ad_id: ad.id,
        cpm: ad.currentBid
      }
    ];
  }

  let updateUrl = `https://api.vk.com/method/ads.updateAds?account_id=${
    ad.cab
  }&data=${JSON.stringify(changes)}&access_token=${token}&v=5.74`;

  const response = await fetch(updateUrl);
  const json = await response.json();
  console.log(`\nRESPONSE UPDATE: ${JSON.stringify(json)}\n`);

  await sleep(2500);
}

async function fetchStat(ad) {
  const today = new Date().toISOString().slice(0, 10);
  const statUrl = `https://api.vk.com/method/ads.getStatistics?account_id=${
    ad.cab
  }&ids_type=ad&ids=${
    ad.id
  }&period=day&date_from=${today}&date_to=0&access_token=${token}&v=5.74`;

  response = await fetch(statUrl);
  json = await response.json();
  console.log(`\nRESPONSE STAT: ${JSON.stringify(json)}\n`);

  stat = json.response[0].stats[0];
  stat.ts = new Date().toLocaleTimeString();
  stat.impressions = stat.impressions ? stat.impressions : 0;
  stat.clicks = stat.clicks ? stat.clicks : 0;
  stat.spent = stat.spent ? stat.spent : 0;
  stat.reach = stat.reach ? stat.reach : 0;
  stat.join_rate = stat.join_rate ? stat.join_rate : 0;

  await sleep(600);
  return stat;
}

async function init() {
  // init stats
  for (const ad of ads) {
    adState = { ...ad };
    adState.prevStat = await fetchStat(ad);
    adState.file = fs.createWriteStream(`logs/${adState.tag}.log`, {
      flags: "a"
    });
    adState.file.write(
      `\n\n\nSTART WATCH\n${JSON.stringify(adState.prevStat)}\n`
    );
    adState.currentBid = 0;
    state[ad.id] = adState;
  }

  console.log(`\n\n${JSON.stringify(state, replacer, 2)}\n\n`);
}

async function stepUp() {
  for (var adId in state) {
    if (state.hasOwnProperty(adId)) {
      const ad = state[adId];
      ad.currentBid = getNextBid(ad);
      console.log(`SET ${ad.tag} BID TO ${ad.currentBid}`);
      await updateAdBid(ad);
    }
  }
}

async function commitStats() {
  for (var adId in state) {
    if (state.hasOwnProperty(adId)) {
      const ad = state[adId];
      let ts = new Date().toLocaleTimeString();
      const prev = ad.prevStat;
      const current = await fetchStat(ad);

      const commit = {
        ts,
        period: `${prev.ts} - ${ts}`,
        bid: ad.currentBid,
        dImpressions: current.impressions - prev.impressions,
        dClicks: current.clicks - prev.clicks,
        dSpent: current.spent - prev.spent,
        dReach: current.reach - prev.reach,
        dJoinRate: current.join_rate - prev.join_rate
      };

      ad.file.write(JSON.stringify(commit) + "\n");
      console.log(`\n${ad.tag}: ${JSON.stringify(commit, null, 2)}\n`);

      ad.prevStat = current;
    }
  }
}

async function startWatch() {
  await init();
  await stepUp();
  setInterval(async () => {
    await commitStats();
    await stepUp();
  }, 900000);
}

startWatch();
